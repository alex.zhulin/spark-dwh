create table if not exists tmp.operations (fkey_transport int, recid int, rectype int, o_id text, o_bln_dt date, o_a_dt text, o_a_tm text, o_dt text, o_tm text, o_cs text, o_dcm_dt text, o_dcm_n text, db_a text, db_a_id text, cr_a text, cr_a_id text, o_amn decimal, db_amn_f decimal, db_amn_f_cd text, cr_amn_f decimal, cr_amn_f_cd text, o_frgn_cd text, o_pmnt text, o_app text, o_csh text, dbe_stt text, dbe_a text, dbe_nm text, dbe_bic text, dbe_swift text, dbe_c_id text, dbe_c_etc text, dbe_c_inn text, dbe_c_kpp text, dbe_c_ogrn text, dbe_c_bic text, dbe_c_swift text, dbe_c_nm text, dbe_c_tp text, dbe_c_bsn text, dbe_o_n text, dbe_c_rsd text, dbe_c_snils text, dbe_c_npoms text, dbe_c_phn text, dbe_c_eml text, cre_stt text, cre_a text, cre_nm text, cre_bic text, cre_swift text, cre_c_id text, cre_c_etc text, cre_c_inn text, cre_c_kpp text, cre_c_ogrn text, cre_c_bic text, cre_c_swift text, cre_c_nm text, cre_c_tp text, cre_c_bsn text, cre_o_n text, cre_c_rsd text, cre_c_snils text, cre_c_npoms text, cre_c_phn text, cre_c_eml text, c_id_r_tp text, c_id_r_srs text, c_id_r_n text, c_id_r_dt_r text, c_id_r_ent_nm text, c_id_r_unt_cd text, c_ntc_stt_cd text, c_ntc_fll text, c_ntc_pst_cd text, c_ntc_lcl_cd text, c_ntc_sbcstt text, c_ntc_lcl text, c_ntc_arlcl text, c_ntc_vll text, c_ntc_strt text, c_ntc_hs text, c_ntc_hsbld text, c_ntc_apr text, c_id_r_tp_1 text, c_id_r_srs_1 text, c_id_r_n_1 text, c_id_r_dt_r_1 text, c_id_r_ent_nm_1 text, c_id_r_unt_cd_1 text, c_ntc_stt_cd_1 text, c_ntc_fll_1 text, c_ntc_pst_cd_1 text, c_ntc_lcl_cd_1 text, c_ntc_sbcstt_1 text, c_ntc_lcl_1 text, c_ntc_arlcl_1 text, c_ntc_vll_1 text, c_ntc_strt_1 text, c_ntc_hs_1 text, c_ntc_hsbld_1 text, c_ntc_apr_1 text, input_file text, rec_timestamp decimal);

FKey_TRANSPORT,RecID,RecType,O_ID,O_BLN_DT,O_A_DT,O_A_TM,O_DT,O_TM,O_CS,O_DCM_DT,O_DCM_N,DB_A,DB_A_ID,CR_A,CR_A_ID,O_AMN,DB_AMN_F,DB_AMN_F_CD,CR_AMN_F,CR_AMN_F_CD,O_FRGN_CD,O_PMNT,O_APP,O_CSH,DBE_STT,DBE_A,DBE_NM,DBE_BIC,DBE_SWIFT,DBE_C_ID,DBE_C_ETC,DBE_C_INN,DBE_C_KPP,DBE_C_OGRN,DBE_C_BIC,DBE_C_SWIFT,DBE_C_NM,DBE_C_TP,DBE_C_BSN,DBE_O_N,DBE_C_RSD,DBE_C_SNILS,DBE_C_NPOMS,DBE_C_PHN,DBE_C_EML,CRE_STT,CRE_A,CRE_NM,CRE_BIC,CRE_SWIFT,CRE_C_ID,CRE_C_ETC,CRE_C_INN,CRE_C_KPP,CRE_C_OGRN,CRE_C_BIC,CRE_C_SWIFT,CRE_C_NM,CRE_C_TP,CRE_C_BSN,CRE_O_N,CRE_C_RSD,CRE_C_SNILS,CRE_C_NPOMS,CRE_C_PHN,CRE_C_EML,C_ID_R_TP,C_ID_R_SRS,C_ID_R_N,C_ID_R_DT_R,C_ID_R_ENT_NM,C_ID_R_UNT_CD,C_NTC_STT_CD,C_NTC_FLL,C_NTC_PST_CD,C_NTC_LCL_CD,C_NTC_SBCSTT,C_NTC_LCL,C_NTC_ARLCL,C_NTC_VLL,C_NTC_STRT,C_NTC_HS,C_NTC_HSBLD,C_NTC_APR,C_ID_R_TP_1,C_ID_R_SRS_1,C_ID_R_N_1,C_ID_R_DT_R_1,C_ID_R_ENT_NM_1,C_ID_R_UNT_CD_1,C_NTC_STT_CD_1,C_NTC_FLL_1,C_NTC_PST_CD_1,C_NTC_LCL_CD_1,C_NTC_SBCSTT_1,C_NTC_LCL_1,C_NTC_ARLCL_1,C_NTC_VLL_1,C_NTC_STRT_1,C_NTC_HS_1,C_NTC_HSBLD_1,C_NTC_APR_1

drop table if exists tmp.operations

create table if not exists tmp.operations (fkey_transport int, recid int, rectype int, o_id text, o_bln_dt date, o_a_dt text, o_a_tm text, o_dt text, o_tm text, o_cs text, o_dcm_dt text, o_dcm_n text, db_a text, db_a_id text, cr_a text, cr_a_id text, o_amn decimal, db_amn_f decimal, db_amn_f_cd text, cr_amn_f decimal, cr_amn_f_cd text, o_frgn_cd text, o_pmnt text, o_app text, o_csh text, dbe_stt text, dbe_a text, dbe_nm text, dbe_bic text, dbe_swift text, dbe_c_id text, dbe_c_etc text, dbe_c_inn text, dbe_c_kpp text, dbe_c_ogrn text, dbe_c_bic text, dbe_c_swift text, dbe_c_nm text, dbe_c_tp text, dbe_c_bsn text, dbe_o_n text, dbe_c_rsd text, dbe_c_snils text, dbe_c_npoms text, dbe_c_phn text, dbe_c_eml text, cre_stt text, cre_a text, cre_nm text, cre_bic text, cre_swift text, cre_c_id text, cre_c_etc text, cre_c_inn text, cre_c_kpp text, cre_c_ogrn text, cre_c_bic text, cre_c_swift text, cre_c_nm text, cre_c_tp text, cre_c_bsn text, cre_o_n text, cre_c_rsd text, cre_c_snils text, cre_c_npoms text, cre_c_phn text, cre_c_eml text, c_id_r_tp text, c_id_r_srs text, c_id_r_n text, c_id_r_dt_r text, c_id_r_ent_nm text, c_id_r_unt_cd text, c_ntc_stt_cd text, c_ntc_fll text, c_ntc_pst_cd text, c_ntc_lcl_cd text, c_ntc_sbcstt text, c_ntc_lcl text, c_ntc_arlcl text, c_ntc_vll text, c_ntc_strt text, c_ntc_hs text, c_ntc_hsbld text, c_ntc_apr text, c_id_r_tp_1 text, c_id_r_srs_1 text, c_id_r_n_1 text, c_id_r_dt_r_1 text, c_id_r_ent_nm_1 text, c_id_r_unt_cd_1 text, c_ntc_stt_cd_1 text, c_ntc_fll_1 text, c_ntc_pst_cd_1 text, c_ntc_lcl_cd_1 text, c_ntc_sbcstt_1 text, c_ntc_lcl_1 text, c_ntc_arlcl_1 text, c_ntc_vll_1 text, c_ntc_strt_1 text, c_ntc_hs_1 text, c_ntc_hsbld_1 text, c_ntc_apr_1 text, input_file text, rec_timestamp decimal);

select *
--delete
from tmp.operations

copy tmp.operations (FKey_TRANSPORT,RecID,RecType,O_ID,O_BLN_DT,O_A_DT,O_A_TM,O_DT,O_TM,O_CS,O_DCM_DT,O_DCM_N,DB_A,DB_A_ID,CR_A,CR_A_ID,O_AMN,DB_AMN_F,DB_AMN_F_CD,CR_AMN_F,CR_AMN_F_CD,O_FRGN_CD,O_PMNT,O_APP,O_CSH,DBE_STT,DBE_A,DBE_NM,DBE_BIC,DBE_SWIFT,DBE_C_ID,DBE_C_ETC,DBE_C_INN,DBE_C_KPP,DBE_C_OGRN,DBE_C_BIC,DBE_C_SWIFT,DBE_C_NM,DBE_C_TP,DBE_C_BSN,DBE_O_N,DBE_C_RSD,DBE_C_SNILS,DBE_C_NPOMS,DBE_C_PHN,DBE_C_EML,CRE_STT,CRE_A,CRE_NM,CRE_BIC,CRE_SWIFT,CRE_C_ID,CRE_C_ETC,CRE_C_INN,CRE_C_KPP,CRE_C_OGRN,CRE_C_BIC,CRE_C_SWIFT,CRE_C_NM,CRE_C_TP,CRE_C_BSN,CRE_O_N,CRE_C_RSD,CRE_C_SNILS,CRE_C_NPOMS,CRE_C_PHN,CRE_C_EML,C_ID_R_TP,C_ID_R_SRS,C_ID_R_N,C_ID_R_DT_R,C_ID_R_ENT_NM,C_ID_R_UNT_CD,C_NTC_STT_CD,C_NTC_FLL,C_NTC_PST_CD,C_NTC_LCL_CD,C_NTC_SBCSTT,C_NTC_LCL,C_NTC_ARLCL,C_NTC_VLL,C_NTC_STRT,C_NTC_HS,C_NTC_HSBLD,C_NTC_APR,C_ID_R_TP_1,C_ID_R_SRS_1,C_ID_R_N_1,C_ID_R_DT_R_1,C_ID_R_ENT_NM_1,C_ID_R_UNT_CD_1,C_NTC_STT_CD_1,C_NTC_FLL_1,C_NTC_PST_CD_1,C_NTC_LCL_CD_1,C_NTC_SBCSTT_1,C_NTC_LCL_1,C_NTC_ARLCL_1,C_NTC_VLL_1,C_NTC_STRT_1,C_NTC_HS_1,C_NTC_HSBLD_1,C_NTC_APR_1, rec_timestamp)
from '/mnt/shared/zhulin/ODOP_1000_0000_F20180403_L20180403_C20180411_0001_UTF_0001_d.csv' csv header delimiter ';'

(select recid, max(rec_timestamp) rec_timestamp
from tmp.operations op_d
where op_d.rectype in (2) -- modified records
group by recid
) op_d

-- Original query

-- New records without modified and deleted ones
select op.recid, op.rectype, op.o_amn
from tmp.operations op
where op.recid not in (select recid
                       from tmp.operations op_d
                       where op_d.rectype in (2, 3) -- modified and deleted records
                       )
union all
-- Modifed actual (with max rec_timestamp) records
select op.recid, op.rectype, op.o_amn
from tmp.operations op
     , (select recid, max(rec_timestamp) rec_timestamp
        from tmp.operations op_d
        where op_d.rectype in (2) -- modified records
        group by recid
        ) op_m
where op.recid = op_m.recid
      and op.rec_timestamp = op_m.rec_timestamp

-- Rewriten query

-- New records without modified and deleted ones
select op.o_id, op.rectype, op.o_amn
from tmp.operations op
     left join (select o_id from tmp.operations where rectype = 2 or rectype = 3) op_d on op.o_id = op_d.o_id
where op.rectype = 1 -- new records
      and op_d.o_id is null
union all
-- Modifed actual (with max rec_timestamp) records
select op.o_id, op.rectype, op.o_amn
from tmp.operations op
     , (select o_id, max(rec_timestamp) rec_timestamp
        from tmp.operations op_d
        where op_d.rectype = 2 -- modified records
        group by o_id
        ) op_m
where op.o_id = op_m.o_id
      and op.rec_timestamp = op_m.rec_timestamp
      