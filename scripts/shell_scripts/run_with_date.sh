#!/bin/bash
cmd=$1
echo $dd
now=$2
echo $now
my_dir="$(dirname "$0")"
sh $my_dir/$cmd "$now"
if [ $? -eq 0 ]
then
  exit 0
else
  exit 1
fi