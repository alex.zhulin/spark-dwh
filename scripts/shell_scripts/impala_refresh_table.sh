echo "Refresh Impala table"
echo $1
ssh platform@eprod-cdh-nn01 "impala-shell -i eprod-cdh-dn01:21000 -q 'refresh $1'"
echo "Impala table refreshed"
echo "Compute stats for Impala table"
echo $2
if [ $2 == "nostats" ]
then
  echo "Compute no stats"
fi
if [ $2 == "computestats" ]
then
  echo "Execute compute stats"
  ssh platform@eprod-cdh-nn01 "impala-shell -i eprod-cdh-dn01:21000 -q 'compute stats $1'"
fi
if [ $2 == "computeincrementalstats" ]
then
  echo "Execute compute incremental stats"
  echo "Partition column"
  echo $3
  echo "Partition value"
  echo $4
  if [ "$#" -ne "2" ]
  then  
    ssh platform@eprod-cdh-nn01 "impala-shell -i eprod-cdh-dn01:21000 -q 'compute incremental stats $1 partition ($3 = \"$4\")'"
  else
    ssh platform@eprod-cdh-nn01 "impala-shell -i eprod-cdh-dn01:21000 -q 'compute incremental stats $1'"
  fi
fi
