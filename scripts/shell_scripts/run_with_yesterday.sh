#!/bin/bash
cmd=$1
my_dir="$(dirname "$0")"
now=$(<$my_dir/run_with_yesterday.txt)

if [ "$now" == "" ]
then
   now=$(date --date="yesterday" +'%Y-%m-%d')
fi
echo $now
sh $my_dir/$cmd "$now"
if [ $? -eq 0 ]
then
  exit 0
else
  exit 1
fi
