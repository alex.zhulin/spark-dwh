#!/bin/bash
dd=$1
echo $dd
spark-submit --class ru.databriz.spark.csvimport.CsvImportRunner --deploy-mode cluster --driver-memory 512M --num-executors 1 --executor-cores 1 --executor-memory 512M --master yarn --verbose /opt/platform/spark/ehdproject-0.1-jar-with-dependencies.jar -inputDate "2018-05-17" -inputFolder "/raw_data"
if [ $? -eq 0 ]
then
  echo "Spark job was successful"
  #sh $my_dir/impala_refresh_table.sh dwh.trades computeincrementalstats
  #sh $my_dir/impala_refresh_table.sh dwh.ccy nostats
  exit 0
else
  echo "Spark job was failed"
  exit 1
fi
