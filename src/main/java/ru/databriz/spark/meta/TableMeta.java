package ru.databriz.spark.meta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Comparator;
import java.util.List;

/**
 * ���������� � ����������� �������
 */
@XmlRootElement(name = "table", namespace = "http://www.databriz.ru/spark/")
@XmlAccessorType(XmlAccessType.NONE)
public class TableMeta {

    @XmlElement(name = "name")
    private String tableName;

    @XmlElement(name = "comment")
    private String tableComment;

    @XmlElement(name = "fileType")
    private String fileType;

    @XmlElement(name = "column")
    private List<TableColumn> columns;

    private TableMeta() {
        this(null, null, null, null);
    }

    public TableMeta(String tableName, String tableComment, String fileType, List<TableColumn> columns) {
        this.tableName = tableName;
        this.tableComment = tableComment;
        this.fileType = fileType;
        this.columns = columns;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public String getFileType() {
        return fileType;
    }

    public List<TableColumn> getColumns() {
        return columns;
    }

    public String[] getPartitions() {
        return getColumns()
                .stream()
                .filter(t -> t.isPartition())
                .map(t -> t.getName())
                .toArray(size -> new String[size]);
    }

    public String getDatePartition() {
        return getColumns()
                .stream()
                .filter(t -> t.isPartition() && (t.getType().equals("date") || t.getName().contains("date")))
                .findFirst()
                .get()
                .getName();
    }

    public String getOrderByFirst() {
        return getColumns()
                .stream()
                .filter(t -> t.getSortOrder() > 0)
                .sorted(Comparator.comparing(t -> t.getSortOrder()))
                .map(t -> t.getName())
                .findFirst()
                .get();
    }

    public String[] getOrderByOthers() {
        return getColumns()
                .stream()
                .filter(t -> t.getSortOrder() > 0)
                .sorted(Comparator.comparing(t -> t.getSortOrder()))
                .map(t -> t.getName())
                .skip(1)
                .toArray(size -> new String[size]);
    }

}
