package ru.databriz.spark.meta;

import javax.xml.bind.annotation.*;

/**
 * ���������� � �������
 */
@XmlRootElement(name = "column")
@XmlAccessorType(XmlAccessType.NONE)
public class TableColumn {

    @XmlElement
    private String name;

    @XmlElement
    private String type;

    @XmlElement
    private String comment;

    @XmlAttribute
    private String function;

    @XmlAttribute
    private boolean partition;

    @XmlAttribute
    private int sortOrder;

    // If field presented in "row level tag" (RecTableOp) then = true, else (it presented in "MainAttributes" tag) = false
    // It used in xml import
    @XmlElement
    private boolean rowLevelAttr;

    private TableColumn() {
        this(null, null, null, null, false, -1, false);
    }

    public TableColumn(String name, String type, String comment, String function, boolean partition, int sortOrder, boolean rowLevelAttr) {
        this.name = name;
        this.type = type;
        this.comment = comment;
        this.function = function;
        this.partition = partition;
        this.sortOrder = sortOrder;
        this.rowLevelAttr = rowLevelAttr;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getComment() {
        return comment;
    }

    public String getFunction() {
        return function;
    }

    public boolean isPartition() {
        return partition;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public boolean isRowLevelAttr() {
        return rowLevelAttr;
    }

    @Override
    public String toString() {
        return name + "(" + type + ")";
    }
}