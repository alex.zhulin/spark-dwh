package ru.databriz.spark.meta;

/**
 * Enum for all csv imported tables
 */
public enum CsvImportTable {

    // Csv table structure
    AccountBalances("CsvImportJobAccountBalances.xml"),
    AccountBlocks("CsvImportJobAccountBlocks.xml"),
    Accounts("CsvImportJobAccounts.xml"),
    Clients("CsvImportJobClients.xml"),
    Operations("CsvImportJobOperations.xml"),
    Registry("CsvImportJobRegistry.xml"),

    // Xml table structure
    OperationsXml("XmlImportJobOperations.xml"),
    RegistryXml("XmlRegistry.xml"),
    ;

    private TableMeta meta;

    CsvImportTable(String fileName) {
        this.meta = TableMetaParser.readFromResource(fileName);
    }

    public TableMeta getMeta() {
        return meta;
    }
}
