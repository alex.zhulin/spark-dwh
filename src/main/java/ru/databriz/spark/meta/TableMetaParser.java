package ru.databriz.spark.meta;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.sql.types.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

/**
 * ������ ��� TableMeta
 */
public class TableMetaParser {

    private static JAXBContext jaxbContext = createJAXBContext();

    private static JAXBContext createJAXBContext() {
        try {
            return JAXBContext.newInstance(TableMeta.class);
        } catch (Exception e) {
            throw new RuntimeException("Cannot instantiate JAXB context: " + e.getMessage());
        }
    }

    public static String marshal(TableMeta checkList) {
        try {
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(checkList, sw);
            return sw.toString();
        } catch (Exception e) {
            throw new RuntimeException("Cannot marshal object: " + e.getMessage());
        }
    }

    private static TableMeta unmarshal(InputStreamReader xmlReader) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(TableMeta.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (TableMeta) unmarshaller.unmarshal(xmlReader);
    }

    public static TableMeta readFromResource(String fileName) {
        ClassLoader classLoader = TableMetaParser.class.getClassLoader();
        InputStream is = classLoader.getResourceAsStream(fileName);
        InputStreamReader isr = new InputStreamReader(is);
        try {
            return unmarshal(isr);
        } catch (Exception e) {
            throw new RuntimeException("Cannot read from resource: " + e.getMessage());
        } finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(is);
        }
    }

    public static StructType getLoaderSchema(TableMeta tableMeta) {
        StructField[] rowLineFields = tableMeta.getColumns()
                .stream()
                .filter(t -> t.getFunction() == null)
                .filter(t -> t.isRowLevelAttr() == true)
                .map(t -> getStructField(t)).toArray(size -> new StructField[size]);

        StructField[] dataFields = tableMeta.getColumns()
                .stream()
                .filter(t -> t.getFunction() == null)
                .filter(t -> t.isRowLevelAttr() == false)
                .map(t -> getStructField(t)).toArray(size -> new StructField[size]);

        StructField mainAttrField = new StructField("MainAttributes", DataTypes.createStructType(dataFields), true, Metadata.empty());
        StructField[] mainAttrs = new StructField[1];
        mainAttrs[0] = mainAttrField;

        StructField[] totalFields = (StructField[]) ArrayUtils.addAll(rowLineFields, mainAttrs);

        return new StructType(
                totalFields
        );
    }

    private static StructField getStructField(TableColumn tableColumn) {
        DataType dataType;
        switch (tableColumn.getType()) {
            case "string":
                dataType = DataTypes.StringType;
                break;
            case "int":
                dataType = DataTypes.IntegerType;
                break;
            case "decimal":
                dataType = DataTypes.DoubleType;
                break;
            case "date":
                dataType = DataTypes.StringType;
                break;
            default:
                dataType = DataTypes.StringType;
                break;
        }
        String xmlColumnName = transformName(tableColumn);
        return new StructField(xmlColumnName, dataType, true, Metadata.empty());
    }

    private static String transformName(TableColumn tableColumn) {
        if ("recid".equalsIgnoreCase(tableColumn.getName())) {
            return "_RecID";
        }
        if ("rectype".equalsIgnoreCase(tableColumn.getName())) {
            return "_RecType";
        }
        return "_" + tableColumn.getName().toUpperCase();
    }
}
