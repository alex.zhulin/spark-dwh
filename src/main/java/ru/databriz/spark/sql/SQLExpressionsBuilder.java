package ru.databriz.spark.sql;

import ru.databriz.spark.meta.TableColumn;
import ru.databriz.spark.meta.TableMeta;

/**
 * Expressions to transform the input dataframe (from csv) to the output one, ready to be inserted to Hive
 */
public class SQLExpressionsBuilder {

    public static String[] getArrayOfSelectExpr(TableMeta tableMeta) {
        return tableMeta.getColumns()
                .stream()
                .map(t -> createExpressionString(t) + " as " + t.getName())
                .toArray(size -> new String[size]);
    }

    private static String createExpressionString(TableColumn t) {
        if (t.getFunction() != null) {
            return t.getFunction();
        } else if (t.getType().equalsIgnoreCase("date")) {
            return "convert_date_format(" + t.getName() + ")";
        } else {
            return "cast(`" + t.getName() + "` as " + t.getType() + ")";
        }
    }
}
