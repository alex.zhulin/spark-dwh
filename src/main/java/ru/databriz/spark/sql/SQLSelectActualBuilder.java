package ru.databriz.spark.sql;

import ru.databriz.spark.meta.TableMeta;

import java.util.stream.Collectors;

/**
 * Builds query to get actual rows (from partitions 1, 2 and 3)
 */
public class SQLSelectActualBuilder {

    /*
with t1 as (
    select * from ${table_name}
	where ${date_partition} = '${date}' and rectype = 1
),
t2_all as (
    select * from ${table_name}
	where ${date_partition} = '${date}' and rectype = 2
),
t2_grouped as (
    select recid, max(rec_timestamp) rec_timestamp from ${table_name}
	where ${date_partition} = '${date}' and rectype = 2
	group by recid
),
t2 as (
    select t2_all.*
    from t2_all join t2_grouped on (t2_all.recid = t2_grouped.recid and t2_all.rec_timestamp = t2_grouped.rec_timestamp)
),
t3 as (
    select * from ${table_name}
	where ${date_partition} = '${date}' and rectype = 3
),
t4 as (
select t1.recid,
       ${coalesce_other_columns}
from t1 left join t2 on (t1.recid = t2.recid)
)
select t4.*
from t4 left join t3 on (t4.recid = t3.recid)
where t3.recid is null
*/

    private static final String SelectTemplate = "with t1 as (\n" +
            "    select * from ${table_name}\n" +
            "\twhere ${date_partition} = '${date}' and rectype = 1\n" +
            "),\n" +
            "t2_all as (\n" +
            "    select * from ${table_name}\n" +
            "\twhere ${date_partition} = '${date}' and rectype = 2\n" +
            "),\n" +
            "t2_grouped as (\n" +
            "    select recid, max(rec_timestamp) rec_timestamp from ${table_name}\n" +
            "\twhere ${date_partition} = '${date}' and rectype = 2\n" +
            "\tgroup by recid\n" +
            "),\n" +
            "t2 as (\n" +
            "    select t2_all.*\n" +
            "    from t2_all join t2_grouped on (t2_all.recid = t2_grouped.recid and t2_all.rec_timestamp = t2_grouped.rec_timestamp)\n" +
            "),\n" +
            "t3 as (\n" +
            "    select * from ${table_name}\n" +
            "\twhere ${date_partition} = '${date}' and rectype = 3\n" +
            "),\n" +
            "t4 as (\n" +
            "select t1.recid,\n" +
            "       ${coalesce_other_columns}\n" +
            "from t1 left join t2 on (t1.recid = t2.recid)\n" +
            ")\n" +
            "select t4.*\n" +
            "from t4 left join t3 on (t4.recid = t3.recid)\n" +
            "where t3.recid is null";

    public static String getSelect(TableMeta tableMeta, String date) {
        assert(tableMeta.getColumns().get(0).getName().equals("recid"));
        return SelectTemplate
                .replace("${table_name}", tableMeta.getTableName())
                .replace("${date_partition}", tableMeta.getDatePartition())
                .replace("${coalesce_other_columns}", getCoalesceOtherColumns(tableMeta))
                .replace("${date}", date);
    }

    private static String getCoalesceOtherColumns(TableMeta tableMeta) {
        return tableMeta.getColumns().stream()
                .skip(1)
                .map(t -> t.getName())
                .map(s -> "coalesce(t2." + s + ", t1." + s + ") as " + s)
                .collect(Collectors.joining(",\n"));
    }
}