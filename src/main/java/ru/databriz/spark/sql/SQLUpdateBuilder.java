package ru.databriz.spark.sql;

import ru.databriz.spark.meta.TableMeta;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Builds queries with changed columns
 */
public class SQLUpdateBuilder {

    public static String getUpdated(TableMeta tableMeta, String partitionFilter, String caseCondition, Map<String, String> changedColumns) {
        StringBuilder sb = new StringBuilder("select ");
        String columns = tableMeta.getColumns()
                .stream()
                .map(c -> c.getName())
                .map(col -> changedColumns.containsKey(col)?
                        "case when " + caseCondition + " then " + changedColumns.get(col) +
                                " else " + col + " end as " + col: col)
                .collect(Collectors.joining(", "));
        sb.append(columns).append(" from ").append(tableMeta.getTableName()).append(" where ").append(partitionFilter);
        return sb.toString();
    }
}
