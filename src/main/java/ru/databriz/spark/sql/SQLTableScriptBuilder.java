package ru.databriz.spark.sql;

import ru.databriz.spark.meta.TableColumn;
import ru.databriz.spark.meta.TableMeta;

import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Builds 'create table' script
 */
public class SQLTableScriptBuilder {

    private static final boolean COMMENT_MODE = false;

    public static String getTableScript(TableMeta tableMeta) {
        // Head of the script
        StringBuilder tableScript = new StringBuilder("create table if not exists ").append(tableMeta.getTableName()).append(" (").append("\n");
        // Add ordinary fields (not partition ones)
        tableScript.append(createColumnsString(tableMeta, t -> !t.isPartition()));

        // Add comment
        tableScript.append(") comment '").append(tableMeta.getTableComment()).append("' ");

        // Add partition fields if exist
        if (tableMeta.getColumns().stream().anyMatch(t -> t.isPartition())) {
            tableScript.append("partitioned by (");
            tableScript.append(createColumnsString(tableMeta, t -> t.isPartition()));
            tableScript.append(") ");
        }
        tableScript.append(" stored as parquet");

        return tableScript.toString();
    }

    private static String createColumnsString(TableMeta tableMeta, Predicate<TableColumn> columnFilter) {
        return tableMeta.getColumns()
                .stream()
                .filter(columnFilter)
                .map(t -> t.getName().toLowerCase() + " " + replaceType(t.getType()) + (COMMENT_MODE? " comment '" + t.getComment() + "'": ""))
                .collect(Collectors.joining(",\n"));
    }

    private static String replaceType(String inType) {
        if (inType.trim().equalsIgnoreCase("date")) {
            return "string";
        } else {
            return inType;
        }
    }

}
