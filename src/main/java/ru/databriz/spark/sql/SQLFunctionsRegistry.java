package ru.databriz.spark.sql;

import org.apache.spark.sql.UDFRegistration;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;

import java.util.Date;

/**
 * Spark user defined functions
 * from here: https://stackoverflow.com/questions/35348058/how-do-i-call-a-udf-on-a-spark-dataframe-using-java
 */
public class SQLFunctionsRegistry {

    protected static final UDF1 convert_date_format = new UDF1<String, String>()
    {
        public String call(String date) throws Exception {
            if (date.isEmpty()) return "";
            if (date.length() < 10) return "";
            return new StringBuilder().append(date.substring(6, 10)).append("-").append(date.substring(3, 5)).append("-").append(date.substring(0, 2)).toString();
        }
    };

    protected static final UDF1 get_file_name = new UDF1<String, String>()
    {
        public String call(String fileName) throws Exception {
            return fileName.substring(fileName.lastIndexOf('/') +1);
        }
    };

    protected static final UDF1 get_import_date = new UDF1<String, String>()
    {
        public String call(String fileName) throws Exception {
            int to = fileName.lastIndexOf('/');
            int from = fileName.lastIndexOf('/', to - 1)+1;
            try {
                return fileName.substring(from, to);
            } catch (Exception e) {
                return "";
            }
        }
    };

    protected static final UDF1 get_org_code = new UDF1<String, String>()
    {
        public String call(String fileName) {
            try {
                return fileName
                        .substring(fileName.lastIndexOf('/') +1)
                        .substring(5, 9);
            } catch (Exception e) {
                return "";
            }
        }
    };

    protected static final UDF1 get_podr_code = new UDF1<String, String>()
    {
        public String call(String fileName) {
            try {
                return fileName
                        .substring(fileName.lastIndexOf('/') + 1)
                        .substring(10, 14);
            } catch (Exception e) {
                return "";
            }
        }
    };

    protected static final UDF1 get_podr_id = new UDF1<String, Integer>()
    {
        public Integer call(String fileName) throws Exception {
            String orgId = get_org_code.call(fileName).toString();
            String podrId = get_podr_code.call(fileName).toString();
            try {
                return Integer.valueOf(orgId + podrId);
            } catch (Exception e) {
                return -1;
            }
        }
    };

    protected static final UDF1<Object, Long> get_rec_timestamp = o -> new Date().getTime();

    public static void registerAll(UDFRegistration udf) {
        udf.register("convert_date_format", convert_date_format, DataTypes.StringType);
        udf.register("get_file_name", get_file_name, DataTypes.StringType);
        udf.register("get_import_date", get_import_date, DataTypes.StringType);
        udf.register("get_org_code", get_org_code, DataTypes.StringType);
        udf.register("get_podr_code", get_podr_code, DataTypes.StringType);
        udf.register("get_podr_id", get_podr_id, DataTypes.IntegerType);
        udf.register("get_rec_timestamp", get_rec_timestamp, DataTypes.LongType);

    }
}
