package ru.databriz.spark.sql;

import ru.databriz.spark.meta.TableColumn;
import ru.databriz.spark.meta.TableMeta;

/**
 * Expressions to transform the input dataframe (from csv) to the output one, ready to be inserted to Hive
 */
public class SQLExpressionsBuilderXml {

    public static String[] getArrayOfSelectExpr(TableMeta tableMeta) {
        return tableMeta.getColumns()
                .stream()
                .map(t -> createExpressionString(t) + " as " + t.getName())
                .toArray(size -> new String[size]);
    }

    private static String createExpressionString(TableColumn t) {
        if (t.getFunction() != null) {
            return t.getFunction();
        } else {
            if (t.getType().equalsIgnoreCase("date")) {
                return getTag(t) + "`_" + t.getName().toUpperCase() + "`";
            } else {
                return "cast(" + getTag(t) + "`_" + t.getName().toUpperCase() + "` as " + t.getType() + ")";
            }
        }
    }

    private static String getTag(TableColumn t) {
        return t.isRowLevelAttr()? "": "MainAttributes.";
    }
}
