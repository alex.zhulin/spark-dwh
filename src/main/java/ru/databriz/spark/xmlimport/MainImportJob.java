package ru.databriz.spark.xmlimport;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function0;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.common.SparkJob;
import ru.databriz.spark.xmlimport.FilePartition;
import ru.databriz.spark.xmlimport.ImportJob;
import ru.databriz.spark.meta.CsvImportTable;
import ru.databriz.spark.sql.SQLSelectActualBuilder;
import ru.databriz.spark.sql.SQLUpdateBuilder;

import java.util.HashMap;

import static ru.databriz.spark.meta.CsvImportTable.*;

/**
 * Для загрузки всех поступивших файлов (описание процедуры загрузки в readme.txt)
 */
public class MainImportJob extends SparkJob {

    protected String inputFolder;
    protected String inputDate;

    private static final Function0<Integer> Constant4 = new Function0<Integer>() {
        @Override
        public Integer call() throws Exception {
            return 4;
        }
    };

    public MainImportJob(SparkEnvironment sparkEnvironment, String inputFolder, String inputDate) {
        super(sparkEnvironment);
        this.inputFolder = inputFolder;
        this.inputDate = inputDate;
    }

    /*
    private void updateRegistryStatusLoaded(String fileType) {
        String datePartition = CsvImportTable.Registry.getMeta().getDatePartition();
        String updated = SQLUpdateBuilder.getUpdated(CsvImportTable.Registry.getMeta(), datePartition + " = '" + inputDate + "'", "file like '" + fileType + "_%'", new HashMap<String,String>() {{
            put("status_loaded", "1");
            put("time_loaded", "current_timestamp()");
        }});

        hiveContext.sql(updated).repartition(1).registerTempTable("updated");
        hiveContext.sql("insert overwrite table registry partition (" + datePartition + ") select * from updated where " + datePartition + " = '" + inputDate + "'");
        hiveContext.refreshTable("registry");
        hiveContext.sql("select * from registry").show();
    }
    */

    private void importTable(CsvImportTable table) {
        ru.databriz.spark.xmlimport.ImportJob importJob = new ru.databriz.spark.xmlimport.ImportJob(sparkEnvironment, table, inputFolder, inputDate);
        importJob.run();
//        updateRegistryStatusLoaded(table.getMeta().getFileType()); // TODO
//        updateFilePartitions(table);
    }

    private void importFilePartitions() {
        JavaRDD<FilePartition> filePartitionRDD = hiveContext.sql("select * from registry").javaRDD().flatMap(FilePartition.GenerateFromRegistryRow);
        DataFrame filePartitions = hiveContext.createDataFrame(filePartitionRDD, FilePartition.class);
        filePartitions.show();

        hiveContext.sql("create table if not exists file_partitions (" +
                "input_file_name string, import_date string, status_accounted tinyint, time_accounted timestamp) " +
                "partitioned by (partition_date string) stored as parquet");

        filePartitions
                .repartition(1)
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .partitionBy("partition_date")
                .insertInto("file_partitions");
    }

    private void updateFilePartitions(CsvImportTable table) {
        // select dates from table file_partitions to update partition 4
        DataFrame dates = hiveContext.sql("select partition_date from file_partitions group by partition_date " +
                "having sum(case when status_accounted = 0 then 1 else 0 end) > 0 order by partition_date");
        for (Row dateRow: dates.collectAsList()) {
            String date = dateRow.getString(0);
            // for each date run SQLSelectActualBuilder query to update partition 4
            DataFrame partition4 = hiveContext.sql(SQLSelectActualBuilder.getSelect(table.getMeta(), date)).cache();
            partition4 = partition4.selectExpr(table.getMeta().getColumns().stream()
                    .map(c -> c.getName().equals("rectype")? "4 as rectype": c.getName())
                    .toArray(count -> new String[count])).repartition(10).cache();
            // check that there is only one partition
            assert partition4.filter("rectype != 4 or " + table.getMeta().getDatePartition() + " != '" + date + "'").count() == 0;
            partition4
                    .write()
                    .format("parquet")
                    .mode(SaveMode.Overwrite)
                    .partitionBy(table.getMeta().getPartitions())
                    .insertInto(table.getMeta().getTableName());
            // update status_accounted in table file_partitions
            hiveContext.sql("insert overwrite table file_partitions partition (partition_date = '" + date + "') " +
                "select input_file_name, import_date, " +
                "case when input_file_name like '" + table.getMeta().getFileType() + "%' then 1 else status_accounted end " +
                "as status_accounted, time_accounted from file_partitions where partition_date = '" + date + "'");
        }
    }

    @Override
    public DataFrame run() {

        importTable(OperationsXml);

        return null;  // TODO fix
//        return hiveContext.sql("select * from registry where file_date = '" + inputDate + "'");
    }

}
