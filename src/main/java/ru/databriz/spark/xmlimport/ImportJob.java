package ru.databriz.spark.xmlimport;

import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.common.SparkJob;
import ru.databriz.spark.meta.CsvImportTable;
import ru.databriz.spark.meta.TableMeta;
import ru.databriz.spark.meta.TableMetaParser;
import ru.databriz.spark.sql.SQLExpressionsBuilderXml;
import ru.databriz.spark.sql.SQLFunctionsRegistry;
import ru.databriz.spark.sql.SQLTableScriptBuilder;

import static ru.databriz.spark.meta.CsvImportTable.RegistryXml;

public class ImportJob extends SparkJob {

    private static final String REGISTRY_TABLE_NAME = "registry";
    private static final Logger log = LoggerFactory.getLogger(ImportJob.class);

    protected String inputFolder;
    protected String inputDate;
    protected TableMeta tableMeta;
    protected DataFrame outputDataFrame = null;

    public ImportJob(SparkEnvironment sparkEnvironment, CsvImportTable csvImportTable, String inputFolder, String inputDate) {
        super(sparkEnvironment);
        this.inputFolder = inputFolder;
        this.inputDate = inputDate;
        this.tableMeta = csvImportTable.getMeta();
    }

    public void dropTable() {
        if (isTest()) {
            hiveContext.sql("DROP TABLE IF EXISTS " + getTableName());
        } else {
            throw new RuntimeException("Drop table is not allowed in a working environment");
        }
    }

    String getPathToDataFile() {
        return hdfsPath() + inputFolder + "/" + inputDate + "/";
    }

    protected String getTableName() {
        return tableMeta.getTableName();
    }

    protected String getFilesMaskXml() {
        return tableMeta.getFileType() + "*.xml";
    }

    private DataFrame loadFromXml(String dir, String fileMask) {

        /*
        DataFrame sample = hiveContext.read()
                .format("com.databricks.spark.xml")
                //.schema(sampleSchema)
                .option("rowTag", "RecTableOp")
                .option("mode", "DROPMALFORMED")
                .option("charset", "windows-1251")
                .load(dir + "sample.xml");
        sample.show();
        //StructType sampleSchema = sample.schema();
        */

        StructType sampleSchema = TableMetaParser.getLoaderSchema(tableMeta);
        sampleSchema.printTreeString();
        return hiveContext.read()
                .format("com.databricks.spark.xml")
                .schema(sampleSchema)
                .option("rowTag", "RecTableOp")
                .option("mode", "DROPMALFORMED")
                .option("charset", "windows-1251")
                .load(dir + fileMask);
    }

    private DataFrame saveToTable(String tableName, String dir, String fileMask) {
        log.info("saveToTable [start]");

        DataFrame dataFrame = loadFromXml(dir, fileMask);
        log.info("saveToTable [loadFromXml(dir=" + dir + ", fileMask = " + fileMask + ")]");
        /*
        dataFrame = dataFrame
                .selectExpr("_RecID as recid"
                        , "MainAttributes._O_ID as o_id"
                        , "convert_date_format(MainAttributes._O_A_DT) as o_a_dt"
                        , "MainAttributes._O_A_TM as o_a_tm"
                        , "MainAttributes._O_DT as o_dt"
                        , "MainAttributes._O_TM as o_tm"
                        , "MainAttributes._O_CS as o_cs"
                        , "MainAttributes._O_DCM_DT as o_dcm_dt"
                        , "MainAttributes._O_DCM_N as o_dcm_n"
                        , "MainAttributes._DB_A as db_a"
                        , "MainAttributes._DB_A_ID as db_a_id"
                        , "MainAttributes._CR_A as cr_a"
                        , "MainAttributes._CR_A_ID as cr_a_id"
                        , "MainAttributes._O_AMN as o_amn"
                        , "MainAttributes._O_APP as o_app"
                        , "MainAttributes._O_CSH as o_csh"
                        , "MainAttributes._DBE_STT as dbe_stt"
                        , "MainAttributes._DBE_NM as dbe_nm"
                        , "MainAttributes._DBE_C_TP as dbe_c_tp"
                        , "MainAttributes._CRE_STT as cre_stt"
                        , "MainAttributes._CRE_A as cre_a"
                        , "_RecType as rectype"
                );
                */

        //dataFrame.printSchema();

        // Add input file name as a column
        dataFrame = dataFrame.withColumn("input_file", functions.input_file_name());
        log.info("saveToTable [dataFrame.withColumn(\"input_file\", functions.input_file_name())]");

        // Process dataFrame in order to get suitable format
        dataFrame = dataFrame.selectExpr(SQLExpressionsBuilderXml.getArrayOfSelectExpr(tableMeta));
        log.info("saveToTable [dataFrame.selectExpr(SQLExpressionsBuilderXml.getArrayOfSelectExpr(tableMeta))]");
        //dataFrame.printSchema();
        //dataFrame.show(10);

        if (!compareTableMeta(dataFrame.schema(), tableMeta)) {
            throw new RuntimeException("Table '" + tableMeta.getTableName() + "' config doesn't correspond to the data: "
                    + dataFrame.schema().toString());
        }

        // Create table if it doesn't exist
        hiveContext.sql(SQLTableScriptBuilder.getTableScript(tableMeta));
        log.info("saveToTable [ hiveContext.sql(SQLTableScriptBuilder.getTableScript(tableMeta))]");

        dataFrame
                .sort(tableMeta.getOrderByFirst(), tableMeta.getOrderByOthers())
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .partitionBy(tableMeta.getPartitions())
                .insertInto(tableName);
        log.info("saveToTable [ dataFrame.sort.write() ... ]");

        if (isTest()) {
            // only for test
            outputDataFrame = dataFrame;
        }
        log.info("saveToTable [finish]");
        return dataFrame;
    }

    private void markFileInRegistry(String tableName, String dir, String fileMask) {
        TableMeta registryMeta = RegistryXml.getMeta();

        //StructType sampleSchema = TableMetaParser.getLoaderSchema(registryMeta);
        //sampleSchema.printTreeString();
        DataFrame dataFrame = hiveContext.read()
                .format("com.databricks.spark.xml")
                //.schema(sampleSchema)
                .option("rowTag", "THEADER")
                .option("mode", "DROPMALFORMED")
                .option("charset", "windows-1251")
                .load(dir + fileMask);
        // Add input file name as a column
        dataFrame = dataFrame.withColumn("input_file", functions.input_file_name());
        dataFrame = dataFrame
                .selectExpr("_nmbbank as nmbbank"
                        , "_regnum as regnum"
                        , "_filial_num as filial_num"
                        , "_date as date"
                        , "_time as time"
                        , "_vsp as vsp"
                        , "_file as file"
                        , "_file_num as file_num"
                        , "_file_num_all as file_num_all"
                        , "PLDate as pldate"
                        , "input_file as input_file_path"
                        , "Bank as bank"
                        , "PBDate as pbdate"
                );
        dataFrame.show(10);
        // Create registry table if it doesn't exist
        hiveContext.sql(SQLTableScriptBuilder.getTableScript(registryMeta));

        dataFrame
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .partitionBy(registryMeta.getPartitions())
                .insertInto(registryMeta.getTableName());
    }

    private boolean compareTableMeta(StructType schema, TableMeta tableMeta) {
        return true;
    }

    @Override
    public DataFrame run() {
        // Register user defined functions
        SQLFunctionsRegistry.registerAll(hiveContext.udf());
        // Get file mask
        String fileMask = getFilesMaskXml();
        // Save file content
        // DataFrame dataFrame = saveToTable(getTableName(), getPathToDataFile(), fileMask);
        // Mark loaded data in registry
        markFileInRegistry(REGISTRY_TABLE_NAME, getPathToDataFile(), fileMask);
        // Return loaded value
        //return dataFrame;
        return null;
    }
}
