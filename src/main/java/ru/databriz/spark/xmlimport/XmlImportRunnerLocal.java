package ru.databriz.spark.xmlimport;

import org.apache.commons.cli.*;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.meta.CsvImportTable;

public class XmlImportRunnerLocal {

    public static void main(String[] args) {
        // create the command line parser
        CommandLineParser parser = new BasicParser();
        Options options = new Options();
        options.addOption(OptionBuilder
                .isRequired(true)
                .hasArg()
                .withDescription("Input date in a format 'YYYY-MM-DD'")
                .create("inputDate"));
        options.addOption(OptionBuilder
                .isRequired(true)
                .hasArg()
                .withDescription("Input folder")
                .create("inputFolder"));
        options.addOption(OptionBuilder
                .isRequired(true)
                .hasArg()
                .withDescription("Table enum name")
                .create("xmlImportTable"));
        try {
            CommandLine line = parser.parse(options, args);
            String inputDate = line.getOptionValue("inputDate");
            String inputFolder = line.getOptionValue("inputFolder");
            String xmlImportTable = line.getOptionValue("xmlImportTable");

            new ImportJob(SparkEnvironment.Local, CsvImportTable.valueOf(xmlImportTable), inputFolder, inputDate)
                    .run();
        } catch (ParseException exp) {
            System.out.println("Unexpected exception:" + exp.getMessage());
        }
    }
}

