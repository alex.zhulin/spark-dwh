package ru.databriz.spark.common;

/*
Тип окружения, в котором выполняется Spark (локальное - для разработки, кластер - продакшн или тестовый кластер)
 */
public enum SparkEnvironment {

    Local,

    Cluster
}
