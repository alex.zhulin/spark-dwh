package ru.databriz.spark.common;

/**
 * Created by Alexey on 05.07.2018.
 */
public enum ImportFormat {
    CSV, XML;
}
