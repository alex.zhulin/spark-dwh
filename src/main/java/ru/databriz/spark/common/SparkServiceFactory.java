package ru.databriz.spark.common;

import java.util.Map;

/*
Для создания SparkService, в зависимости от sparkEnvironment
 */
public class SparkServiceFactory {

    private static SparkService sparkServiceInstance = null;

    protected static SparkService getOrCreateSparkService(SparkEnvironment sparkEnvironment, Map<String, String> additionalConf) {
        if (sparkServiceInstance != null) {
            return sparkServiceInstance;
        }
        sparkServiceInstance = createNewSparkService(sparkEnvironment, additionalConf);
        return sparkServiceInstance;
    }

    private static SparkService createNewSparkService(SparkEnvironment sparkEnvironment, Map<String, String> additionalConf) {
        switch (sparkEnvironment) {
            case Local: return new SparkServiceLocal(additionalConf);
            case Cluster: return new SparkServiceCluster(additionalConf);
//            case LocalPerfTest: return new SparkServiceLocalPerf(additionalConf);
//            case LocalWithRemoteHDFS: return new SparkServiceLocalWithRemoteHDFS(additionalConf);
        }
        throw new IllegalArgumentException("Not implemented: " + sparkEnvironment); // TODO
    }
}
