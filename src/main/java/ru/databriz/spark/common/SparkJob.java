package ru.databriz.spark.common;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import java.util.Collections;
import java.util.Map;

/*
Содержит все необходимые объекты (sparkContext, hiveContext) для запуска Spark программы.
Весь код для выполнения Spark должен содержаться в реализациях этого абстрактного класса (метод run())
 */
public abstract class SparkJob {

    protected final SparkEnvironment sparkEnvironment;

    protected final SparkService sparkService;

    protected final JavaSparkContext sc;

    protected final HiveContext hiveContext;

    protected final SQLContext sqlContext;

    public SparkJob(SparkEnvironment sparkEnvironment) {
        this.sparkEnvironment = sparkEnvironment;
        this.sparkService = SparkServiceFactory.getOrCreateSparkService(sparkEnvironment, getAdditionalProperties());
        this.sc = sparkService.sc;
        this.hiveContext = sparkService.hiveContext;
        this.sqlContext = sparkService.sqlContext;
    }

    protected Map<String, String> getAdditionalProperties() {
        return Collections.emptyMap();
    }

    public abstract DataFrame run();

    protected String hdfsPath() {
        return sparkService.hdfsPath();
    }

    // TODO temporary for development
    public HiveContext getHiveContext() {
        return hiveContext;
    }

    public SQLContext getSqlContext() {
        return sqlContext;
    }

    public boolean isTest() {
        return sparkEnvironment.equals(SparkEnvironment.Local);
    }
}
