package ru.databriz.spark.common;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import java.util.Map;

public abstract class SparkService {

    private SparkConf conf;

    protected final JavaSparkContext sc;

    protected final SQLContext sqlContext;

    protected final HiveContext hiveContext;

    SparkService(Map<String, String> additionalConfig) {
        this.conf = createSparkConfig();
        //conf.set("spark.yarn.executor.memoryOverhead", "1024");
        conf.set("spark.kryoserializer.buffer.max", "128m");

        for (Map.Entry<String, String> pair: additionalConfig.entrySet()) {
            conf.set(pair.getKey(), pair.getValue());
        }

        // conf.registerKryoClasses(); TODO: registerKryoClasses

        // TODO : resetLogger, registerTempTable

        //sc = JavaSparkContext.getOrCreate(conf);
        sc = new JavaSparkContext(conf);
        sqlContext = new SQLContext(sc);
        hiveContext = new HiveContext(sc);

        // See avro params: https://apache.googlesource.com/avro/+/trunk/lang/java/mapred/src/main/java/org/apache/avro/mapreduce/AvroKeyOutputFormat.java
        // See config params example: https://github.com/prateek/hadoop-fileformat-benchmark-kit/blob/master/generate-conversion-hql.sh
        hiveContext.setConf("hive.exec.dynamic.partition", "true");
        hiveContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict");
        hiveContext.setConf("hive.exec.compress.output", "true");
        //    if (parCodec == "avro") {
        //      hiveContext.setConf("avro.output.codec", "deflate")
        //      hiveContext.setConf("avro.mapred.deflate.level", parCompression)
        //    }
        //    if (parCodec == "parquet") {
        hiveContext.setConf("parquet.compression",  "GZIP");
        sqlContext.setConf("spark.sql.parquet.compression.codec", "GZIP");
    }

    protected abstract SparkConf createSparkConfig();

    protected abstract String hdfsPath();

    protected abstract String hivePath();

    protected abstract String getCurrentApplicationId();

    protected abstract void cleanUp();
}
