package ru.databriz.spark.common;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/*
 Реализация SparkService для локальной разработки/отладки/запуска юнит-тестов
 */
public class SparkServiceLocal extends SparkService {

    private int applicationId = 0;

    public SparkServiceLocal(Map<String, String> additionalConfig) {
        super(additionalConfig);
        System.out.println("Local Spark context is initialized. Please make sure that the actual contents of NHDF.zip is in 'c:/user/hdfs/' folder");

    }

    private void cleanUpLocal() {
        if (!Files.exists(FileSystems.getDefault().getPath(hdfsPath())))
            throw new RuntimeException("Cannot initialize local Spark context for unit tests! Please copy the contents of NHDF.zip to 'c:/user/hdfs/' folder");
        try {
            FileUtils.deleteDirectory(new File(hivePath()));
            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            FileUtils.deleteDirectory(new File(s + "/metastore_db"));
        } catch (Exception e) {
            throw new RuntimeException("Cannot delete local hive directory");
        }
    }

    protected SparkConf createSparkConfig() {
        cleanUpLocal();
        SparkConf conf = new SparkConf()
                .setMaster("local[1]")
                .setAppName("localSparkJob");
        conf.set("spark.local.dir", "/tmp/hive/spark");
        conf.set("spark.default.parallelism", "16");  // TODO in production need to make it = number of executors * 2
        conf.set("spark.sql.shuffle.partitions", "16");
        conf.set("spark.executor.memory", "512m");
        return conf;
    }

    protected String hdfsPath() {
        return "/user/hdfs";
    }

    protected String hivePath() {
        return "/user/hive/warehouse/";
    }

    protected String getCurrentApplicationId() {
        return String.valueOf(applicationId);
    }

    protected void cleanUp() {
        // TODO implement for unit tests
    }
}
