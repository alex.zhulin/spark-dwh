package ru.databriz.spark.common;

import org.apache.spark.SparkConf;

import java.util.Map;
import java.util.Properties;

/*
Реализация SparkService для запуска на кластере
 */
public class SparkServiceCluster extends SparkService {

    Properties properties = new Properties();

    public SparkServiceCluster(Map<String, String> additionalConfig) {
        super(additionalConfig);

    }

    protected SparkConf createSparkConfig() {
        return new SparkConf();
    }

    @Override
    protected String hdfsPath() {
        return "hdfs://fds-ehd2:8020"; // TODO - вынести в конфиг
    }

    @Override
    protected String hivePath() {
        return hdfsPath() + "/user/hive/warehouse/";
    }

    @Override
    protected String getCurrentApplicationId() {
        return null;
    }

    @Override
    protected void cleanUp() {

    }
}
