package ru.databriz.spark.csvimport;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Row;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Entity for file_partitions table.
 */
public class FilePartition {

    static List<String> ImportDatesCalendar = new LinkedList<>();

    static {
        LocalDate startDate = LocalDate.of(2018, Month.JANUARY, 1);
        LocalDate endDate = LocalDate.of(2030, Month.JANUARY, 1);

        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        List<String> dates = IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(i -> startDate.plusDays(i))
                .map(t -> t.format(DateTimeFormatter.ISO_LOCAL_DATE))
                .collect(Collectors.toList());
        ImportDatesCalendar.addAll(dates);
    }

    static FlatMapFunction GenerateFromRegistryRow = new FlatMapFunction<Row, FilePartition>() {

        private String getDate(String s, int startFrom) {
            return s.substring(startFrom, startFrom + 4) + "-" + s.substring(startFrom + 4, startFrom + 6) + "-" + s.substring(startFrom + 6, startFrom + 8);
        }

        @Override
        public Iterable<FilePartition> call(Row row) throws Exception {
            String registryFileName = row.getAs("input_file_name");
            String from = getDate(registryFileName, registryFileName.indexOf("_F") + 2);
            String to = getDate(registryFileName, registryFileName.indexOf("_L") + 2);

            return ImportDatesCalendar.subList(ImportDatesCalendar.indexOf(from), ImportDatesCalendar.indexOf(to)+1)
                    .stream()
                    .map(partDate -> new FilePartition(row.getAs("file"), row.getAs("import_date"), (byte) 0, null, partDate))
                    .collect(Collectors.toList());
        }
    };

    private String input_file_name;

    private String import_date;

    private Byte status_accounted;

    private Timestamp time_accounted;

    private String partition_date;

    public FilePartition(String input_file_name, String import_date, Byte status_accounted, Timestamp time_accounted, String partition_date) {
        this.input_file_name = input_file_name;
        this.import_date = import_date;
        this.status_accounted = status_accounted;
        this.time_accounted = time_accounted;
        this.partition_date = partition_date;
    }

    public String getInput_file_name() {
        return input_file_name;
    }

    public String getImport_date() {
        return import_date;
    }

    public Byte getStatus_accounted() {
        return status_accounted;
    }

    public Timestamp getTime_accounted() {
        return time_accounted;
    }

    public String getPartition_date() {
        return partition_date;
    }
}
