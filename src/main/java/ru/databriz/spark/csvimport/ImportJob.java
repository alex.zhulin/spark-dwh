package ru.databriz.spark.csvimport;

import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;
import ru.databriz.spark.common.ImportFormat;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.common.SparkJob;
import ru.databriz.spark.meta.CsvImportTable;
import ru.databriz.spark.meta.TableMeta;
import ru.databriz.spark.sql.SQLExpressionsBuilder;
import ru.databriz.spark.sql.SQLFunctionsRegistry;
import ru.databriz.spark.sql.SQLTableScriptBuilder;

import java.util.ArrayList;
import java.util.List;

/*
����������� ����� ��� �������� ������ �� CSV.
��� ���������� ����������� �� XML, �� �� ������ ������������ SQL ��� �������� ������� � ����������� �������� �� CSV.
���������� �������� �������� ����� � ������, �������� �������, ��������.
 */
public class ImportJob extends SparkJob {

    protected String inputFolder;
    protected String inputDate;
    protected TableMeta tableMeta;
    protected DataFrame outputDataFrame = null;

    public ImportJob(SparkEnvironment sparkEnvironment, CsvImportTable csvImportTable, String inputFolder, String inputDate) {
        super(sparkEnvironment);
        this.inputFolder = inputFolder;
        this.inputDate = inputDate;
        this.tableMeta = csvImportTable.getMeta();
    }

    public void dropTable() {
        if (isTest()) {
            hiveContext.sql("DROP TABLE IF EXISTS " + getTableName());
        } else {
            throw new RuntimeException("Drop table is not allowed in a working environment");
        }
    }

    String getPathToDataFile() {
        return hdfsPath() + inputFolder + "/" + inputDate + "/";
    }

    protected String getTableName() {
        return tableMeta.getTableName();
    }

    protected String getFilesMaskCsv() {
        return tableMeta.getFileType() + "_*_d.csv";
    }

    protected String getFilesMaskXml() {
        return tableMeta.getFileType() + "*.xml";
    }

    private DataFrame loadFromCsv(String pathToData) {
        return hiveContext.read()
                .format("com.databricks.spark.csv")
                .option("header", "true")
                .option("mode", "DROPMALFORMED")
                .option("delimiter", ";")
                .option("comment", "@")
                .option("charset", "windows-1251")
                .load(pathToData);
    }

    private DataFrame saveToTable(String tableName, String pathToData) {
        SQLFunctionsRegistry.registerAll(hiveContext.udf());

        DataFrame dataFrame = loadFromCsv(pathToData);
        if (!compareTableMeta(dataFrame.schema(), tableMeta)) {
            throw new RuntimeException("Table '" + tableMeta.getTableName() + "' config doesn't correspond to the data: "
                    + dataFrame.schema().toString());
        }

        // Create table if it doesn't exists
        hiveContext.sql(SQLTableScriptBuilder.getTableScript(tableMeta));

        // Add input file name as a column
        dataFrame = dataFrame.withColumn("input_file", functions.input_file_name());

        // Process dataFrame in order to get suitable format
        dataFrame = dataFrame.selectExpr(SQLExpressionsBuilder.getArrayOfSelectExpr(tableMeta));

        dataFrame.printSchema();
        dataFrame.show(10);

        dataFrame
                .sort(tableMeta.getOrderByFirst(), tableMeta.getOrderByOthers())
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .partitionBy(tableMeta.getPartitions())
                .insertInto(tableName);

        if (isTest()) {
            // only for test
            outputDataFrame = dataFrame;
        }
        return dataFrame;
    }

    private boolean compareTableMeta(StructType schema, TableMeta tableMeta) {
        return true;
    }

    @Override
    public DataFrame run() {
        String fileMask = getFilesMaskCsv();
        return saveToTable(getTableName(), getPathToDataFile() + fileMask);
    }

    public void checkInsertedAndSelected() {
        // TODO Move to unit tests?
        if (isTest()) {
            DataFrame inserted = outputDataFrame;
            DataFrame selected = hiveContext.sql("select * from " + getTableName() +
                    " where " + tableMeta.getDatePartition() + " = '" + inputDate + "'");
            List<Row> selectedList = new ArrayList<>(selected.collectAsList());

            selectedList.removeAll(inserted.collectAsList());
            if (selectedList.size() > 0) {
                System.out.println("Selected:");
                selected.show();
                System.out.println("Inserted:");
                inserted.show();
                throw new RuntimeException("There is a difference between inserted and selected dataframes.");
            }
        }
    }
}
