package ru.databriz.spark.csvimport;

import org.apache.commons.cli.*;
import ru.databriz.spark.common.ImportFormat;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.meta.CsvImportTable;

public class CsvImportRunner {

    public static void main(String[] args) {
        // create the command line parser
        CommandLineParser parser = new BasicParser();
        Options options = new Options();
        options.addOption(OptionBuilder
                .isRequired(true)
                .hasArg()
                .withDescription("Input date in a format 'YYYY-MM-DD'")
                .create("inputDate"));
        options.addOption(OptionBuilder
                .isRequired(true)
                .hasArg()
                .withDescription("Input folder")
                .create("inputFolder"));
        options.addOption(OptionBuilder
                .isRequired(true)
                .hasArg()
                .withDescription("Table enum name")
                .create("csvImportTable"));
        try {
            CommandLine line = parser.parse(options, args);
            String inputDate = line.getOptionValue("inputDate");
            String inputFolder = line.getOptionValue("inputFolder");
            String csvImportTable = line.getOptionValue("csvImportTable");

            new ImportJob(SparkEnvironment.Cluster, CsvImportTable.valueOf(csvImportTable), inputFolder, inputDate)
                    .run();
        } catch (ParseException exp) {
            System.out.println("Unexpected exception:" + exp.getMessage());
        }
    }
}

