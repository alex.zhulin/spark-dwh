package com.databricks.spark.xml.parsers

class StructTypeMap[A <: AnyRef, B] extends scala.collection.mutable.HashMap[A, B] {

  protected override def elemEquals(key1: A, key2: A): Boolean = (key1 eq key2)

  protected override def elemHashCode(key: A): Int = {
    System.identityHashCode(key)
  }
}
