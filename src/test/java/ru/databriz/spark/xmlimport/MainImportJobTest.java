package ru.databriz.spark.xmlimport;

import org.apache.spark.sql.DataFrame;
import org.junit.Test;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.csvimport.HiveCleanupTest;

public class MainImportJobTest extends HiveCleanupTest {

    @Test
    public void runMainJob() {
        DataFrame res = new MainImportJob(SparkEnvironment.Local, "/unittest_changed_records", "2018-06-05").run();
//        Assert.assertTrue(res.filter("status_loaded = 0").count() == 0);
    }
}
