package ru.databriz.spark.csvimport;

import org.junit.Before;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.meta.CsvImportTable;

/**
 * Clean up Hive warehouse before every test run
 */
public class HiveCleanupTest {

    @Before
    public void cleanUp() {
        for (CsvImportTable table: CsvImportTable.values()) {
            new ImportJob(SparkEnvironment.Local, table, "/unittest_changed_records", "2018-04-11").dropTable();
        }
    }
}
