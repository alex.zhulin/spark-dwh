package ru.databriz.spark.csvimport;

import org.apache.spark.sql.DataFrame;
import org.junit.Test;
import ru.databriz.spark.common.ImportFormat;
import ru.databriz.spark.common.SparkEnvironment;
import ru.databriz.spark.meta.CsvImportTable;
import ru.databriz.spark.meta.TableMeta;
import ru.databriz.spark.sql.SQLSelectActualBuilder;

public class CsvImportJobTest extends HiveCleanupTest {

    @Test
    public void testSimple() {
        ImportJob job = new ImportJob(SparkEnvironment.Local, CsvImportTable.Registry, "/unittest_changed_records", "2018-04-11");
        job.run();
    }

    private void testTableWithCheck(CsvImportTable csvImportTable) {
        ImportJob job = new ImportJob(SparkEnvironment.Local, csvImportTable, "/unittest_changed_records", "2018-04-11");
        job.run();
        job.checkInsertedAndSelected();
    }

    @Test
    public void testClients() {
        testTableWithCheck(CsvImportTable.Clients);
    }

    @Test
    public void testAccounts() {
        testTableWithCheck(CsvImportTable.Accounts);
    }

    @Test
    public void testAccountBlocks() {
        testTableWithCheck(CsvImportTable.AccountBlocks);
    }

    @Test
    public void testRegistry() {
        testTableWithCheck(CsvImportTable.Registry);
    }

    @Test
    public void testAccountBalances() {
        testTableWithCheck(CsvImportTable.AccountBalances);
    }

    @Test
    public void testOperations() {
        testTableWithCheck(CsvImportTable.Operations);
    }

    @Test
    public void testChangedRecords() {
        TableMeta tableMeta = CsvImportTable.AccountBalances.getMeta();
        ImportJob job = new ImportJob(SparkEnvironment.Local, CsvImportTable.AccountBalances, "/unittest_changed_records", "2018-04-11");
        job.run();
        job.checkInsertedAndSelected();

        job = new ImportJob(SparkEnvironment.Local, CsvImportTable.AccountBalances, "/unittest_changed_records", "2018-04-12");
        job.run();

        DataFrame df = job.getHiveContext().sql(SQLSelectActualBuilder.getSelect(tableMeta, "2018-04-02"));
        df.show();
        // clean up

        // TODO add assert checks
    }

}
