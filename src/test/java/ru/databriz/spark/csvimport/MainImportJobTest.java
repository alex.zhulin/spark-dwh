package ru.databriz.spark.csvimport;

import org.apache.spark.sql.DataFrame;
import org.junit.Assert;
import org.junit.Test;
import ru.databriz.spark.common.ImportFormat;
import ru.databriz.spark.common.SparkEnvironment;

public class MainImportJobTest extends HiveCleanupTest {

    @Test
    public void runMainJob() {
        DataFrame res = new MainImportJob(SparkEnvironment.Local, "/unittest_changed_records", "2018-04-11").run();
        Assert.assertTrue(res.filter("status_loaded = 0").count() == 0);
    }
}
