package ru.databriz.spark.meta;

import org.junit.Assert;
import org.junit.Test;

public class TableMetaParserTest {

    @Test
    public void testReadFromResource() {
        TableMeta tableMeta;

        tableMeta = TableMetaParser.readFromResource("CsvImportJobAccountBalances.xml");
        Assert.assertNotNull(tableMeta);

        tableMeta = TableMetaParser.readFromResource("CsvImportJobAccountBlocks.xml");
        Assert.assertNotNull(tableMeta);

        tableMeta = TableMetaParser.readFromResource("CsvImportJobAccounts.xml");
        Assert.assertNotNull(tableMeta);

        tableMeta = TableMetaParser.readFromResource("CsvImportJobClients.xml");
        Assert.assertNotNull(tableMeta);

        tableMeta = TableMetaParser.readFromResource("CsvImportJobOperations.xml");
        Assert.assertNotNull(tableMeta);

        tableMeta = TableMetaParser.readFromResource("CsvImportJobRegistry.xml");
        Assert.assertNotNull(tableMeta);
    }
}
